import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	
	username:  string ='';

	isUsernameEmpty(): boolean {
		if (this.username.length < 1) {
			return true;
		} else {
			return false;
		}
	}

	getUsername(): string{
		return this.username;
	}

	resetUsername(): string {
		this.username = '';
	}
}
